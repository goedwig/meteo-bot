import logging
import sqlite3


class Database:
    logger = logging.getLogger(__name__)

    class Decorators:
        @staticmethod
        def log(function):
            def wrapper(self, *args, **kwargs):
                try:
                    return function(self, *args, **kwargs)
                except sqlite3.DatabaseError:
                    self.logger.error(f'DatabaseError executing {function.__name__}{args} {kwargs}', exc_info=True)

            return wrapper

    def __init__(self, database_file_path):
        self.__connection = sqlite3.connect(database_file_path, check_same_thread=False)
        self.__connection.row_factory = self.__dict_factory

    @staticmethod
    def __dict_factory(cursor, row):
        d = dict()
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    @Decorators.log
    def close_connection(self):
        self.__connection().close()

    @Decorators.log
    def get_stats(self, start_date, end_date):
        cursor = self.__connection.cursor().execute(
            """
            SELECT timestamp, temperature, pressure, humidity
            FROM stats
            WHERE DATE(timestamp) BETWEEN ? AND ?
            """,
            (start_date, end_date)
        )
        return cursor.fetchall()

    @Decorators.log
    def get_extreme_values(self):
        cursor = self.__connection.cursor().execute(
            """
            SELECT 
                MAX(temperature) AS temperature_max, MIN(temperature) AS temperature_min, 
                MAX(pressure) AS pressure_max, MIN(pressure) AS pressure_min, 
                MAX(humidity) AS humidity_max, MIN(humidity) AS humidity_min
            FROM stats
            """)
        return cursor.fetchone()
