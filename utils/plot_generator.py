import datetime as dt
import logging
import os

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as mdates


matplotlib.use('Agg')

logger = logging.getLogger(__name__)


class PlotGenerator:
    TITLES = {
        'temperature': 'температури повітря',
        'pressure': 'атмосферного тиску',
        'humidity': 'вологості повітря'
    }

    YLABELS = {
        'temperature': 'Температура повітря, °C',
        'pressure': 'Атмосферний тиск, hPa',
        'humidity': 'Вологість повітря, % rH'
    }

    LEGEND_LABELS = {
        'temperature': 'Температура повітря',
        'pressure': 'Атмосферний тиск',
        'humidity': 'Вологість повітря'
    }

    LINE_COLORS = {
        'temperature': 'r',
        'pressure': 'g',
        'humidity': 'b'
    }

    @staticmethod
    def generate_plot(measure, dates, values):
        if not dates or not values:
            return None

        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax1.set_title(f'Показники {PlotGenerator.TITLES[measure]} в офісі')

        # Configure x-ticks
        ax1.xaxis.set_major_formatter(mdates.DateFormatter('%d.%m.%Y %H:%M'))

        # Plot temperature data on left Y axis
        ax1.set_ylabel(PlotGenerator.YLABELS[measure])
        ax1.plot_date(PlotGenerator._convert_dates(dates), values, '-',
                      label=PlotGenerator.LEGEND_LABELS[measure], color=PlotGenerator.LINE_COLORS[measure])

        # Format the x-axis for dates (label formatting, rotation)
        fig.autofmt_xdate(rotation=60)
        fig.tight_layout()

        # Show grids and legends
        ax1.grid(True)
        ax1.legend(loc='best', framealpha=0.5)

        filename = os.path.join(os.path.dirname(__file__), '..', 'images',
                                f'{measure}_{dt.datetime.now().isoformat()}.png')
        plt.savefig(filename)

        return filename

    @staticmethod
    def _convert_dates(dates):
        return [mdates.datestr2num(d) for d in dates]
