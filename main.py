import logging
import os

from core import ClimateStation, Bot
from utils import Config, Database

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
logger = logging.getLogger(__name__)


def main():
    config = Config(os.path.join(os.path.dirname(__file__), 'config', 'config.json'))
    database = Database(os.path.join(os.path.dirname(__file__), '..', 'climate_stats.db'))
    climate_station = ClimateStation()

    bot = Bot(config, climate_station, database)

    bot.run()


if __name__ == '__main__':
    main()
