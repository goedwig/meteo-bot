import logging
import datetime as dt

from telegram import ParseMode, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from utils import PlotGenerator

logger = logging.getLogger(__name__)

SELECT_MEASURE, SELECT_DATE_RANGE = range(2)


class Bot:
    def __init__(self, config, climate_station, database):
        self.__config = config
        self.__climate_station = climate_station
        self.__database = database

    @staticmethod
    def _start(update, context):
        context.bot.send_message(
            update.message.chat_id,
            'Привіт! Я — бот для взаємодії з кліматичною станцією 🤖\n'
            'Виконай команду /stats, щоб отримати поточні показники.\n'
            'Команда /help допоможе дізнатись більше про те, на що я здатний!',
            parse_mode=ParseMode.MARKDOWN
        )

    def _stats(self, update, context):
        data = self.__climate_station.sample()

        context.bot.send_message(
            update.message.chat_id,
            f'🌦 *Показники станом на {data.timestamp:%H:%M %d.%m.%Y}*\n\n'
            f'Температура повітря: `{data.temperature:.2f}°C`\n'
            f'Атмосферний тиск: `{data.pressure:.2f}hPa`\n'
            f'Вологість повітря: `{data.humidity:.2f}% rH`',
            parse_mode=ParseMode.MARKDOWN
        )

        logger.info(data)

    @staticmethod
    def _help(update, context):
        context.bot.send_message(
            update.message.chat_id,
            '*Що я вмію?*\n\n'
            '/start — ініціалізація роботи з ботом\n\n'
            '/stats — отримати показники температури повітря, вологості та атмосферного тиску\n\n'
            '/history — отримати історію вимірів температури повітря, вологості та атмосферного тиску у '
            'вигляді графіків\n\n'
            '/extreme — отримати найвищі на найнижчі зафіксовані значення вимірів температури повітря, вологості '
            'та атмосферного тиску\n\n'
            '/help — допомога по використанню бота\n\n'
            'Контакти для зв\'язку: @goedwig, @amicum, @darnahorna',
            parse_mode=ParseMode.MARKDOWN
        )

    @staticmethod
    def _cancel(update, context):
        context.bot.send_message(update.message.chat_id, 'Відміна!')
        return ConversationHandler.END

    @staticmethod
    def _history(update, context):
        keyboard = [
            [InlineKeyboardButton('Температура повітря', callback_data='temperature')],
            [InlineKeyboardButton('Атмосферний тиск', callback_data='pressure')],
            [InlineKeyboardButton('Вологість повітря', callback_data='humidity')],
        ]

        context.bot.send_message(
            update.message.chat_id,
            '📑 Які історичні дані ви хотіли б отримати?',
            reply_markup=InlineKeyboardMarkup(keyboard)
        )

        return SELECT_MEASURE

    @staticmethod
    def _measure_selection(update, context):
        measure = update.callback_query.data
        context.user_data['measure'] = measure

        context.bot.send_message(
            update.callback_query.message.chat_id,
            '📅 Вкажіть період для отримання історичних даних в форматі:\n\n`01.01.2020-02.03.2020`\n\n'
            'Або ви можете використати наступні команди:\n\n'
            '/today — отримати дані за сьогодні\n\n'
            '/yesterday — отримати дані за вчора\n\n'
            '/week — отримати дані за останні 7 днів',
            parse_mode=ParseMode.MARKDOWN
        )

        return SELECT_DATE_RANGE

    def _date_range_selection(self, update, context):
        msg = update.message.text
        measure = context.user_data.get('measure')

        if msg == '/today':
            start_date = end_date = dt.date.today()
        elif msg == '/yesterday':
            start_date = end_date = dt.date.today() - dt.timedelta(days=1)
        elif msg == '/week':
            end_date = dt.date.today()
            start_date = end_date - dt.timedelta(days=7)
        else:
            try:
                start_date, end_date = (dt.datetime.strptime(d, '%d.%m.%Y').date() for d in msg.split('-'))
                if start_date > end_date:
                    context.bot.send_message(
                        update.message.chat_id,
                        'Кінцева дата має бути більшою або дорівнювати початковій. Будь ласка, спробуйте ще раз 😥'
                    )
                    return
            except ValueError:
                context.bot.send_message(
                    update.message.chat_id,
                    'Невірний формат. Будь ласка, спробуйте ще раз 😥'
                )
                return

        context.bot.send_message(
            update.message.chat_id,
            'Працюю... 🤖'
        )

        dates, values = [], []
        for row in self.__database.get_stats(start_date, end_date):
            dates.append(row['timestamp'])
            values.append(row[measure])

        filename = PlotGenerator.generate_plot(measure, dates, values)

        if filename:
            context.bot.send_photo(
                update.message.chat_id,
                photo=open(filename, 'rb'))

            return ConversationHandler.END
        else:
            context.bot.send_message(
                update.message.chat_id,
                'На жаль, дані за вказаний проміжок часу відсутні 🤷‍♀️'
            )

    def _extreme(self, update, context):
        stats = self.__database.get_extreme_values()

        context.bot.send_message(
            update.message.chat_id,
            '🔝 *Найвищі та найнижчі зареєстровані значення вимірів*\n\n'
            '*Температура повітря:*\n'
            f'Min: `{stats["temperature_min"]:.2f}°C`\n'
            f'Max: `{stats["temperature_max"]:.2f}°C`\n\n'
            '*Атмосферний тиск:*\n'
            f'Min: `{stats["pressure_min"]:.2f}hPa`\n'
            f'Max: `{stats["pressure_max"]:.2f}hPa`\n\n'
            '*Вологість повітря:*\n'
            f'Min: `{stats["humidity_min"]:.2f}% rH`\n'
            f'Max: `{stats["humidity_max"]:.2f}% rH`',
            parse_mode=ParseMode.MARKDOWN
        )

    def run(self):
        updater = Updater(self.__config['bot']['token'], use_context=True)
        dp = updater.dispatcher

        dp.add_handler(CommandHandler('start', self._start))
        dp.add_handler(CommandHandler('stats', self._stats))
        dp.add_handler(CommandHandler('extreme', self._extreme))
        dp.add_handler(CommandHandler('help', self._help))

        historical_conv_handler = ConversationHandler(
            entry_points=[CommandHandler('history', self._history)],
            states={
                SELECT_MEASURE: [CallbackQueryHandler(self._measure_selection, pass_user_data=True)],
                SELECT_DATE_RANGE: [
                    MessageHandler(Filters.text & ~Filters.command, self._date_range_selection, pass_user_data=True),
                    CommandHandler('today', self._date_range_selection, pass_user_data=True),
                    CommandHandler('yesterday', self._date_range_selection, pass_user_data=True),
                    CommandHandler('week', self._date_range_selection, pass_user_data=True)
                ]
            },
            fallbacks=[CommandHandler('cancel', self._cancel)]
        )
        dp.add_handler(historical_conv_handler)

        updater.start_polling()
        logger.info('Bot has been started')
        updater.idle()
