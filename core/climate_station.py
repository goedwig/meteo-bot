import smbus2
import bme280


class ClimateStation:
    def __init__(self, port=1, address=0x76):
        self.__port = port
        self.__address = address
        self.__bus = smbus2.SMBus(port)
        self.__calibration_params = bme280.load_calibration_params(self.__bus, address)

    def sample(self):
        data = bme280.sample(self.__bus, self.__address, self.__calibration_params)
        return data
